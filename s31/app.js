//[SECTION] Dependencies and Modules
const express = require("express");
const mongoose =  require("mongoose");
const taskRoute = require("./routes/taskRoute");

//[SECTION] Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//[SECTION] Database Connection
mongoose.connect('mongodb+srv://Djbarcoma:admin123@cluster0.0hs3z.mongodb.net/todo176?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//Add the task route
//Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
//localhost:4000/tasks/
app.use("/tasks", taskRoute);

//[SECTION] Entry Point Response
app.listen(port, () => console.log(`Now listening to port ${port}`));
