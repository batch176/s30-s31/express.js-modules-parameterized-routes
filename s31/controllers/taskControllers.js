//Controllers contains the functions and business logic of our Express JS Application

//Uses the "require" directive to allow access to the "Task" model which allows us to access mongoose methods to perform CRUD functions
//Allows us to use the contents of the "task.js" file in the "models" folder
const task = require("../models/task");
const { findByIdAndDelete, findByIdAndRemove, findById } = require("../models/task");
const Task = require("../models/task");

//[CONTROLLERS]


//[RETRIEVE]
module.exports.getAllTasks = () => {

	//The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function the "/tasks" routes is accessed

	return Task.find({}).then(result => {

		//The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result
	})
}



//[CREATE]
//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {


	//Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({

		//Sets the "name" property with the value received from the client/Postman
		name: requestBody.name
	})


// Saves the newly created "newTask" object in the MongoDB database
// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
// The "then" method will accept the following 2 arguments:
	// The first parameter will store the result returned by the Mongoose "save" method
	// The second parameter will store the "error" object
// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
	// Ex.
		// newUser.save((saveErr, savedTask) => {})
	return newTask.save().then((task, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error) {
			return false
		// Save successful, returns the new task object back to the client/Postman
		} else {
			return task
		}
	})
}


//[DESTROY] - REMOVE EXISTING RESOURCES INSIDE THE COLLECTION
	//expose data across other modules other app so that it will become reusable

module.exports.deleteTask = (idNgTask) => {
	//how will the data will be processed in order to execute the task.
	//select which mongoose method will be used in order to acquire the desired end goal.
	//mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongo DB atlas
	//function will be executed  append the model name.

	//findByIdAndRemove => this is a mongoose method which target a document using its id field and remmove the targeted document from the collection.
	
	//Promise in JS
	//=> Pending (waiting to be executed)
	//=> Fulfilled (successfully executed)
	//=> Rejected (unfilfilled promise)

	//we are going to insert a 'thenable' expression to determin HOW we will respond depending on the result of the promise.

	//identify the 2 possible state of the promise

	return Task.findByIdAndRemove(idNgTask).then((fulfilled, rejected) => {
		
		if (fulfilled) {
			return 'The Task has been successfully Removed';
		} else {
			return 'Failed to remove Task';
		}
	});
};


//UPDATE
	//1. Change tha status of Task. pending -> complete

module.exports.taskCompleted = (taskId => {

	return Task.findById(taskId).then((found, error) =>{

		if (found) {
			console.log(found)
			found.status = 'Completed';

			return found.save().then ((updatedTask, saveErr) => {
				if (updatedTask) {
					return 'Task has been successfully modified';
				} else {
					return 'Task failed to Update';
				}
			})

		} else {
			return 'Error! No Match Found';
		}

	});

});

//2. Change the status of task (Completed -> pending)
module.exports.taskPending =  (userInput) => {
		return Task.findById(userInput).then((result, err) => {
			
			if (result) {

				result.status = 'Pending';
				return result.save().then((taskUpdated, error) => {
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`;
					} else {
						return 'Error when saving task updates';
					}
				})
			} else {
				return 'Something Went Wrong';
			}

		})
	}